module SafiraTypetester

  class Expression < SyntaxNode
    def type
      tipo_primeiro_elemento = @elements[0].type
      #tipo do restante
      elementos_que_diferem = @elements.select {|el| el.type != tipo_primeiro_elemento}
      if elementos_que_diferem.count > 0
        raise "Erro de tipo no noh #{@text_value}, offset #{@offset}"
      else
        return tipo_primeiro_elemento
      end
    end
  end
  
end