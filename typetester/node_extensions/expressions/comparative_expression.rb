module SafiraTypetester

  class ComparativeExpression < SyntaxNode
    def type
      #se o elemento da esquerda eh comparavel com o da direita (mesmo tipo)
      if @elements[0].type == @elements[2].type
        return 'boolean'
      else
        raise "Erro de tipo no noh #{@text_value}, offset #{@offset} " + elementos_que_diferem.to_s
      end
    end
  end
  
end