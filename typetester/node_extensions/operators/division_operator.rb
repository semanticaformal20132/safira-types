module SafiraTypetester

  class DivisionOperator < SyntaxNode
    def type
      #o segundo operando nao pode ser 0
      raise "Erro de divisao inteira por zero no noh #{@text_value}, offset #{@offset}" if @parent.elements[2].text_value == '0'

      #o tipo dos operandos deve ser o mesmo
      if @parent.elements[0].type == @parent.elements[2].type
        return @parent.elements[0].type
      else
        raise "Erro de tipo no noh #{@text_value}, offset #{@offset}"
      end
    end
  end
  
end