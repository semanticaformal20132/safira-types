module SafiraTypetester

  class LessThanOperator < SyntaxNode
    def type
      #o tipo dos operandos deve ser o mesmo
      if @parent.elements[0].type == @parent.elements[2].type
        return 'boolean'
      else
        raise "Erro de tipo no noh #{@text_value}, offset #{@offset}"
      end
    end
  end
  
end