module SafiraTypetester

  class FunctionDefinition < SyntaxNode
    def type
      if @elements[0].type == 'identifier'
        #o bloco do THEN deve ser unit
        if @elements[1].type == 'unit'
            return 'unit'
        else
            raise "Tipo do Bloco da funcao #{@offset} da expressao #{@text_value} eh invalida"
        end
      return 'identifier'
      else
          raise "Tipo do IDENTIFICADOR da funcao #{@offset} da expressao #{@text_value} eh invalida"
      end
    end
  end 
end