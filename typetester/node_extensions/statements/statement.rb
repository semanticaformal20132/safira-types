module SafiraTypetester

  class Statement < SyntaxNode
    def type
      @elements[0].type
    end
  end
  
end