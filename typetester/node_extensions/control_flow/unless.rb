module SafiraTypetester

  class Unless < SyntaxNode
    def type
      #a primeira expressao do if deve ser boolean
      if @elements[0].type == 'boolean'
        #o bloco do THEN deve ser unit
        if @elements[1].type == 'unit'
          #o ELSE eh opcional
          unless @elements[2].nil?
            #mas se existir, deve ser tambem unit
            if @elements[2].type == 'unit'
              return 'unit'
            else
              raise "Tipo do ELSE no if #{@offset} da expressao #{@text_value} eh invalida"
            end
            return 'unit'
          end
          return 'unit'
        else
          raise "Tipo do THEN no if #{@offset} da expressao #{@text_value} eh invalida"
        end
      else
        raise "Tipo do teste booleano no if #{@offset} da expressao #{@text_value} nao eh boolean"
      end
    end
  end
end