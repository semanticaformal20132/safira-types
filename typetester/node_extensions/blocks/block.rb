module SafiraTypetester

  class Block < SyntaxNode
    def type
      @valid_types = ['integer','boolean','unit','float','string']
      #percorre todos statements do bloco para verificar se retornaram algum tipo compativel
      @elements.each do |element|
         unless @valid_types.include? element.type
           raise "Tipagem errada em um dos elementos raiz do bloco"
         end
      end
      return 'unit'
    end
  end
  
end