module SafiraTypetester

  class FalseLiteral < SyntaxNode
    def type
      'boolean'
    end
  end
  
end