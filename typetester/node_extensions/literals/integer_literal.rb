module SafiraTypetester

  class IntegerLiteral < SyntaxNode
    def type
      'integer'
    end
  end
  
end