module SafiraTypetester

  class NilLiteral < SyntaxNode
    def type
      'unit'
    end
  end
  
end