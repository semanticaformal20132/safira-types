module SafiraTypetester

  class TrueLiteral < SyntaxNode
    def type
      'boolean'
    end
  end
  
end