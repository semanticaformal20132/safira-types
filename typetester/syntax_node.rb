module SafiraTypetester

  class SyntaxNode

    attr_accessor :name, :parent, :elements, :offset, :text_value

    def initialize(name, text_value, offset, elements = nil)
      @name = name
      @parent = nil
      @elements = elements
      @offset = offset
      @text_value = text_value
      unless( elements.nil? )
        elements.each do |element|
          element.parent = self
        end
      end
    end

  end

end