module SafiraTypetester

  class Analyser

    def self.type(ast_hash)
      self.load_ast_hash(ast_hash).type
    end

    #Converte a minha arvore simplificada para subclasse do no especifico (superclasse syntax_node)
    def self.load_ast_hash(hash)
      unless(hash[:elements].nil?)
        elements = hash[:elements].map {|x| self.load_ast_hash(x) }
      else
        elements = nil
      end
      @tree = Kernel.const_get("SafiraTypetester").const_get(hash[:name]).new(hash[:name],hash[:text_value], hash[:offset], elements)
      return @tree
    end

  end

end