#Parser da Linguagem Safira

É uma linguagem fictícia criada para demonstrar os conceitos aprendidos na cadeira de Semântica Formal, no Instituto de Informática da UFRGS.

Essa linguagem utiliza inferência de tipos para determinar a correta tipagem, em cada um dos literais,
 comandos, etc. Para isso, em nossa implementação, usamos a linguagem Ruby, com a biblioteca Treetop,
 para construir uma AST. Após isso, utilizando essa AST, construímos um analisador de tipos para essa
 árvore, que é percorrida nó-a-nó, e cada um dos seus nós pode prosseguir para seus filhos, utilizar nós irmãos,
 ou ainda o nó pai, dependendo da parte do código que se está analisando.


 ---  Instalação do Ruby e Treetop em Ubuntu Linux

\curl -L https://get.rvm.io | bash -s stable --ruby
rvm use 2.0.0
gem install treetop

 ---  Instalação do Ruby e Treetop em Windows

 Baixar a ultima versao em http://rubyinstaller.org/downloads/
 Instalar

 No prompt de comando instalar a gem treetop com:

gem install treetop


Para rodar nosso código utilizar, na raiz do projeto:

ruby safira-parser.rb NOMEDOARQUIVOSEMTXT

Basta colocar os arquivos de teste na pasta testes com a extensao txt
Note que para rodar o analisador não vai o .txt do final