=begin
	Arquivo que somente carrega todos arquivos da pasta parser
=end

require 'rubygems'

require_files = []
require_files.concat Dir[File.join(File.dirname(__FILE__), 'parser', '**', '*.rb')]

require_files.each do |file|
  require File.expand_path(file)
end

#puts ARGV[0]
arquivo_teste = File.open("testes/"+ARGV[0]+".txt")

arvore_ast = Parser.parse(arquivo_teste.read)
arvore_ast_hash = arvore_ast.to_hash
p arvore_ast

#agora o analisador de tipos
require File.expand_path(File.join(File.dirname(__FILE__), 'typetester/syntax_node.rb'))
require_files = []
require_files.concat Dir[File.join(File.dirname(__FILE__), 'typetester', '**', '*.rb')]

require_files.each do |file|
  require File.expand_path(file)
end



type = SafiraTypetester::Analyser.type(arvore_ast_hash)
if type == 'unit'
  puts "Analise de tipos concluida com sucesso #{type}"
else
  puts "Deveria ter dado algum raise ate esse ponto"
end