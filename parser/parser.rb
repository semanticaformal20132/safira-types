class Parser

  require 'treetop'
  Treetop.load(File.expand_path(File.join(File.dirname(__FILE__), 'safira.treetop')))
  @@parser = SafiraParser.new
  
  def self.parse(data)
    tree = @@parser.parse(data)
    
    if(tree.nil?)
      raise ParseError, "Erro de parsing no offset: #{@@parser.index}"
    end
    
    # Limpa a arvore de nos desnecessarios gerados pelo treetop
    self.clean_tree(tree)
    
    return tree
  end
  
  private
  
  #somente removo o SyntaxNode... as outras propriedades, como text_value, offset e etc usarei no processamento de tipos
    def self.clean_tree(root_node)
      #raiz nao nula?
      return if(root_node.elements.nil?)
      #verifico se na raiz tem
      root_node.elements.delete_if{|node| node.class.name == "Treetop::Runtime::SyntaxNode" }
      #verifico nos filhos recursivamente
      root_node.elements.each {|node| self.clean_tree(node) }
    end

end